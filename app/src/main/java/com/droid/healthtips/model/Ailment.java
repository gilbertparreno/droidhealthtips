package com.droid.healthtips.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

public class Ailment implements Parcelable {
  @SerializedName("name")
  private String name;

  @SerializedName("definition")
  private String definition;

  @SerializedName("causes")
  private List<Causes> causesList;

  @SerializedName("symptoms")
  private List<String> symptoms;

  @SerializedName("treatments")
  private List<String> treatments;

  @SerializedName("doctor")
  private List<String> doctor;

  @SerializedName("preventions")
  private List<String> preventions;

  @SerializedName("references")
  private List<String> references;

  private String id;

  @SerializedName("suggestion")
  private String suggestion;

  public Ailment() {
    this.id = UUID.randomUUID().toString();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(String definition) {
    this.definition = definition;
  }

  public List<String> getSymptoms() {
    return symptoms;
  }

  public void setSymptoms(List<String> symptoms) {
    this.symptoms = symptoms;
  }

  public List<String> getTreatments() {
    return treatments;
  }

  public void setTreatments(List<String> treatments) {
    this.treatments = treatments;
  }

  public List<String> getDoctor() {
    return doctor;
  }

  public void setDoctor(List<String> doctor) {
    this.doctor = doctor;
  }

  public List<String> getPreventions() {
    return preventions;
  }

  public void setPreventions(List<String> preventions) {
    this.preventions = preventions;
  }

  public List<String> getReferences() {
    return references;
  }

  public void setReferences(List<String> references) {
    this.references = references;
  }

  public List<Causes> getCausesList() {
    return causesList;
  }

  public void setCausesList(List<Causes> causesList) {
    this.causesList = causesList;
  }

  public String getSuggestion() {
    return suggestion;
  }

  public void setSuggestion(String suggestion) {
    this.suggestion = suggestion;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.name);
    dest.writeString(this.definition);
    dest.writeTypedList(this.causesList);
    dest.writeStringList(this.symptoms);
    dest.writeStringList(this.treatments);
    dest.writeStringList(this.doctor);
    dest.writeStringList(this.preventions);
    dest.writeStringList(this.references);
    dest.writeString(this.id);
    dest.writeString(this.suggestion);
  }

  protected Ailment(Parcel in) {
    this.name = in.readString();
    this.definition = in.readString();
    this.causesList = in.createTypedArrayList(Causes.CREATOR);
    this.symptoms = in.createStringArrayList();
    this.treatments = in.createStringArrayList();
    this.doctor = in.createStringArrayList();
    this.preventions = in.createStringArrayList();
    this.references = in.createStringArrayList();
    this.id = in.readString();
    this.suggestion = in.readString();
  }

  public static final Parcelable.Creator<Ailment> CREATOR =
      new Parcelable.Creator<Ailment>() {
        @Override
        public Ailment createFromParcel(Parcel source) {
          return new Ailment(source);
        }

        @Override
        public Ailment[] newArray(int size) {
          return new Ailment[size];
        }
      };
}

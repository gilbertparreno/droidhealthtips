package com.droid.healthtips.model;

public class MenuModel {
  private String label;
  private boolean isGroup;
  private boolean hasChildren;
  private int menuIcon;
  private Ailment ailment;
  private String assetPath;

  public MenuModel(String label, boolean isGroup, boolean hasChildren, int menuIcon) {
    this.label = label;
    this.isGroup = isGroup;
    this.hasChildren = hasChildren;
    this.menuIcon = menuIcon;
  }

  public MenuModel(String label, boolean isGroup, boolean hasChildren, String assetPath) {
    this.label = label;
    this.isGroup = isGroup;
    this.hasChildren = hasChildren;
    this.assetPath = assetPath;
  }

  public MenuModel(
      String label, boolean isGroup, boolean hasChildren, String link, Ailment ailment) {
    this.label = label;
    this.isGroup = isGroup;
    this.hasChildren = hasChildren;
    this.ailment = ailment;
  }

  public String getLabel() {
    return label;
  }

  public boolean isGroup() {
    return isGroup;
  }

  public boolean isHasChildren() {
    return hasChildren;
  }

  public int getMenuIcon() {
    return menuIcon;
  }

  public Ailment getAilment() {
    return ailment;
  }

  public String getAssetPath() {
    return assetPath;
  }
}

package com.droid.healthtips.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Causes implements Parcelable {
  @SerializedName("header")
  private String header;

  @SerializedName("items")
  private List<String> items;

  public Causes() {}

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public List<String> getItems() {
    return items;
  }

  public void setItems(List<String> items) {
    this.items = items;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.header);
    dest.writeStringList(this.items);
  }

  protected Causes(Parcel in) {
    this.header = in.readString();
    this.items = in.createStringArrayList();
  }

  public static final Parcelable.Creator<Causes> CREATOR =
      new Parcelable.Creator<Causes>() {
        @Override
        public Causes createFromParcel(Parcel source) {
          return new Causes(source);
        }

        @Override
        public Causes[] newArray(int size) {
          return new Causes[size];
        }
      };
}

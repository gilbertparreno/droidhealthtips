package com.droid.healthtips.views.main;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.droid.healthtips.R;
import com.droid.healthtips.model.MenuModel;
import com.droid.healthtips.util.MenuUtil;
import com.droid.healthtips.views.ailment_details.AilmentDetailActivity;
import com.droid.healthtips.views.first_aid.FirstAidActivity;

public class MainActivity extends AppCompatActivity {

  private final String TAG = this.getClass().getSimpleName();

  private DrawerLayout drawerLayout;
  private View content;
  private ExpandableMenuAdapter adapter;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main_drawer);

    content = findViewById(R.id.content);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    drawerLayout = findViewById(R.id.drawerLayout);

    ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    ActionBarDrawerToggle mDrawerToggle;
    mDrawerToggle =
        new ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {

          public void onDrawerClosed(View drawerView) {
            supportInvalidateOptionsMenu();
            super.onDrawerClosed(drawerView);
          }

          public void onDrawerOpened(View drawerView) {
            supportInvalidateOptionsMenu();
            super.onDrawerOpened(drawerView);
          }

          @Override
          public void onDrawerSlide(View drawerView, float slideOffset) {
            super.onDrawerSlide(drawerView, slideOffset);
            content.setTranslationX(slideOffset * drawerView.getWidth());
            drawerLayout.bringChildToFront(drawerView);
            drawerLayout.requestLayout();
          }
        };
    mDrawerToggle.setDrawerIndicatorEnabled(true);
    drawerLayout.addDrawerListener(mDrawerToggle);
    mDrawerToggle.syncState();
    prepareMenu();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      drawerLayout.openDrawer(GravityCompat.START);
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void prepareMenu() {
    MenuUtil menuUtil = new MenuUtil();

    ExpandableListView expandableMenu = findViewById(R.id.expandableMenu);

    adapter =
        new ExpandableMenuAdapter(
            menuUtil.getParent(), menuUtil.getSecondLevel(), menuUtil.getThirdLevel());

    expandableMenu.setAdapter(adapter);

    expandableMenu.setChildDivider(
        ContextCompat.getDrawable(getApplicationContext(), R.color.menuDividerColor));

    expandableMenu.setOnChildClickListener(
        (parent, v, groupPosition, childPosition, id) -> {
          MenuModel menuModel = (MenuModel) adapter.getChild(groupPosition, childPosition);
          if (menuModel.getAilment() != null) {
            Intent intent = new Intent(MainActivity.this, AilmentDetailActivity.class);
            intent.putExtra(AilmentDetailActivity.EXTRA_AILMENT, menuModel.getAilment());
            startActivity(intent);
          } else {
            Intent intent = new Intent(MainActivity.this, FirstAidActivity.class);
            intent.putExtra(FirstAidActivity.EXTRA_ASSET_PATH, menuModel.getAssetPath());
            intent.putExtra(FirstAidActivity.EXTRA_TITLE, menuModel.getLabel());
            startActivity(intent);
          }
          return true;
        });

    // this will automatically collapse menu
    //        var prevGroup = -1
    //        expandableMenu.setOnGroupExpandListener {
    //            if (it != prevGroup) {
    //                expandableMenu.collapseGroup(prevGroup)
    //            }
    //
    //            prevGroup = it
    //        }
  }
}

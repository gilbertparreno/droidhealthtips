package com.droid.healthtips.views.first_aid;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.droid.healthtips.R;
import com.droid.healthtips.util.Utils;

public class FirstAidActivity extends AppCompatActivity {

  public static final String EXTRA_TITLE = "title";
  public static final String EXTRA_ASSET_PATH = "asset_path";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_first_aid);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);

    Intent intent = getIntent();

    if (!intent.hasExtra(EXTRA_TITLE) && intent.hasExtra(EXTRA_ASSET_PATH)) {
      throw new IllegalArgumentException(
          "EXTRA_TITLE and EXTRA_ASSET_PATH must pass via intent bundle.");
    }

    String title = intent.getStringExtra(EXTRA_TITLE);
    String path = intent.getStringExtra(EXTRA_ASSET_PATH);

    getSupportActionBar().setTitle(title);
    TextView textView = findViewById(R.id.tvFirstAid);
    textView.setText(Html.fromHtml(Utils.getStringFromAsset(getApplicationContext(), path)));
    textView.setMovementMethod(new LinkMovementMethod());
  }
}

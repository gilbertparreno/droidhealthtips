package com.droid.healthtips.views.ailment_details;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.BulletSpan;
import android.view.MenuItem;
import android.widget.TextView;

import com.droid.healthtips.R;
import com.droid.healthtips.model.Ailment;
import com.droid.healthtips.model.Causes;

import java.util.List;

public class AilmentDetailActivity extends AppCompatActivity {

  public static final String EXTRA_AILMENT = "ailment";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_ailment_detail);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);

    if (!getIntent().hasExtra(EXTRA_AILMENT)) {
      throw new IllegalArgumentException("EXTRA_AILMENT_ID must pass via intent bundle.");
    }

    Ailment ailment = getIntent().getParcelableExtra(EXTRA_AILMENT);
    initViews(ailment);

    getSupportActionBar().setTitle(ailment.getName());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      finish();
    }

    return super.onOptionsItemSelected(item);
  }

  private void initViews(Ailment ailment) {
    TextView tvDesc = findViewById(R.id.tvDesc);
    tvDesc.setText(ailment.getDefinition());

    TextView tvSymptoms = findViewById(R.id.tvSymptoms);
    tvSymptoms.setText(Html.fromHtml(convertListToString(ailment.getSymptoms(), false)));

    TextView tvTreatment = findViewById(R.id.tvTreatment);
    tvTreatment.setText(Html.fromHtml(convertListToString(ailment.getTreatments(), false)));

    TextView tvDoctor = findViewById(R.id.tvDoctor);
    tvDoctor.setText(Html.fromHtml(convertListToString(ailment.getDoctor(), false)));

    TextView tvPreventions = findViewById(R.id.tvPreventions);
    tvPreventions.setText(Html.fromHtml(convertListToString(ailment.getPreventions(), false)));

    TextView tvReferences = findViewById(R.id.tvReferences);
    tvReferences.setText(Html.fromHtml(convertListToString(ailment.getReferences(), false)));
    tvReferences.setMovementMethod(new LinkMovementMethod());

    TextView tvSuggestion = findViewById(R.id.tvSuggestion);
    tvSuggestion.setText(ailment.getSuggestion());

    TextView tvCauses = findViewById(R.id.tvCauses);
    StringBuilder builder = new StringBuilder();
    for (Causes cause : ailment.getCausesList()) {
      boolean addBold = false;
      String items = "";
      if (cause.getItems().size() > 0) {
        items = convertListToString(cause.getItems(), true);
        addBold = true;
      }

      if (addBold) {
        builder.append("&#8226; <b>" + cause.getHeader() + "</b><br/>");
      } else {
        builder.append("&#8226; " + cause.getHeader() + "<br/>");
      }

      builder.append(items);
    }
    tvCauses.setText(Html.fromHtml(builder.toString()));
  }

  private String convertListToString(List<String> items, boolean addTab) {
    StringBuilder builder = new StringBuilder();
    for (String s : items) {
      if (addTab) {
        builder.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
      }
      builder.append("&#8226; " + s + "<br/>");
    }

    return builder.toString();
  }
}

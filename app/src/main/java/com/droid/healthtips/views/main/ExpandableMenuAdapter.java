package com.droid.healthtips.views.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.droid.healthtips.R;
import com.droid.healthtips.model.MenuModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableMenuAdapter extends BaseExpandableListAdapter {

  private List<MenuModel> parent;
  private HashMap<MenuModel, List<MenuModel>> secondMenu;
  private HashMap<MenuModel, List<MenuModel>> thirdMenu;
  private boolean isSubMenu;

  public ExpandableMenuAdapter(
      List<MenuModel> parent,
      HashMap<MenuModel, List<MenuModel>> secondMenu,
      HashMap<MenuModel, List<MenuModel>> thirdMenu,
      boolean isSubMenu) {
    this.parent = parent;
    this.secondMenu = secondMenu;
    this.thirdMenu = thirdMenu;
    this.isSubMenu = isSubMenu;
  }

  public ExpandableMenuAdapter(
      List<MenuModel> parent,
      HashMap<MenuModel, List<MenuModel>> secondMenu,
      HashMap<MenuModel, List<MenuModel>> thirdMenu) {
    this.parent = parent;
    this.secondMenu = secondMenu;
    this.thirdMenu = thirdMenu;
  }

  @Override
  public int getGroupCount() {
    return parent.size();
  }

  @Override
  public int getChildrenCount(int groupPosition) {
    MenuModel menu = parent.get(groupPosition);
    return !menu.isHasChildren() ? 0 : secondMenu.get(menu).size();
  }

  @Override
  public Object getGroup(int groupPosition) {
    return parent.get(groupPosition);
  }

  @Override
  public Object getChild(int groupPosition, int childPosition) {
    MenuModel menu = parent.get(groupPosition);
    MenuModel subMenu = secondMenu.get(menu).get(childPosition);
    return subMenu;
  }

  @Override
  public long getGroupId(int groupPosition) {
    return groupPosition;
  }

  @Override
  public long getChildId(int groupPosition, int childPosition) {
    return childPosition;
  }

  @Override
  public boolean hasStableIds() {
    return false;
  }

  @Override
  public View getGroupView(
      int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
    MenuModel menu = (MenuModel) getGroup(groupPosition);
    String label = menu.getLabel();
    View view = convertView;
    if (view == null) {
      LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
      view =
          layoutInflater.inflate(
              isSubMenu ? R.layout.view_menu_item : R.layout.view_menu_group, parent, false);
      if (isSubMenu) {
        int padLeft = view.getPaddingLeft() * 2;
        view.setPadding(
            padLeft, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
      }
    }

    TextView textView = (TextView) view;
    textView.setText(label);

    int d = 0;
    if (menu.isHasChildren()) {
      if (isExpanded) {
        d = R.drawable.vec_expand_less;
      } else {
        d = R.drawable.vec_expand_more;
      }
    }

    textView.setCompoundDrawablesWithIntrinsicBounds(menu.getMenuIcon(), 0, d, 0);

    return view;
  }

  @Override
  public View getChildView(
      int groupPosition,
      int childPosition,
      boolean isLastChild,
      View convertView,
      ViewGroup parent) {

    MenuModel menu = this.parent.get(groupPosition);
    String label = secondMenu.get(menu).get(childPosition).getLabel();
    View view;

    List<MenuModel> t = thirdMenu.get(secondMenu.get(menu).get(childPosition));
    if (t == null || t.size() == 0) {
      LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
      view = layoutInflater.inflate(R.layout.view_menu_item, parent, false);
      TextView textView = (TextView) view;

      int padLeft = textView.getPaddingLeft() * (isSubMenu ? 3 : 2);
      view.setPadding(
          padLeft,
          textView.getPaddingTop(),
          textView.getPaddingRight(),
          textView.getPaddingBottom());

      textView.setText(label);
    } else {
      MultiLevelMenuView subExpandableListView = new MultiLevelMenuView(parent.getContext());
      MenuModel secondMenu = this.secondMenu.get(menu).get(childPosition);
      List<MenuModel> multiLevelMenu = thirdMenu.get(secondMenu);
      //          secondMenu = hashMapOf(pairs = Pair(secondMenu, multiLevelMenu))
      List<MenuModel> p = new ArrayList<>();
      p.add(secondMenu);

      HashMap<MenuModel, List<MenuModel>> subItems = new HashMap<>();
      subItems.put(secondMenu, multiLevelMenu);

      ExpandableMenuAdapter adapter =
          new ExpandableMenuAdapter(
              p, subItems, new HashMap<MenuModel, List<MenuModel>>(), isSubMenu = true);
      subExpandableListView.setAdapter(adapter);
      view = subExpandableListView;
    }

    return view;
  }

  @Override
  public boolean isChildSelectable(int groupPosition, int childPosition) {
    return true;
  }
}

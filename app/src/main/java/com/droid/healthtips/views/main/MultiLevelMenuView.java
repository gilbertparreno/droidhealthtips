package com.droid.healthtips.views.main;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ExpandableListView;

public class MultiLevelMenuView extends ExpandableListView {
  public MultiLevelMenuView(Context context) {
    super(context);
  }

  public MultiLevelMenuView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public MultiLevelMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    setGroupIndicator(null);
    setDividerHeight(
        (int)
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 0.5f, getResources().getDisplayMetrics()));
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(
        widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(999999, View.MeasureSpec.AT_MOST));
  }
}

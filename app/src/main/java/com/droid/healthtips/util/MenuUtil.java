package com.droid.healthtips.util;

import com.droid.healthtips.App;
import com.droid.healthtips.R;
import com.droid.healthtips.model.Ailment;
import com.droid.healthtips.model.MenuModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MenuUtil {
  private List<MenuModel> parent;
  private HashMap<MenuModel, List<MenuModel>> secondLevel;
  private HashMap<MenuModel, List<MenuModel>> thirdLevel;

  public MenuUtil() {
    parent = new ArrayList<>();
    secondLevel = new HashMap<>();
    thirdLevel = new HashMap<>();

    parent.add(new MenuModel("Ailment", true, true, R.drawable.ic_menu_show));
    List<MenuModel> firstMenu = new ArrayList<>();
    for (Ailment a : App.getInstance().getAilments()) {
      firstMenu.add(new MenuModel(a.getName(), false, false, "www.google.com", a));
    }
    secondLevel.put(parent.get(parent.size() - 1), firstMenu);

    //    parent.add(
    //        new MenuModel("Things To Do", true, true, "www.google.com", R.drawable.ic_menu_todo));

    //    MenuModel animalEncounter = new MenuModel("Animal Encounter", false, true,
    // "www.google.com");
    //    List<MenuModel> animalEncounterSubItems = new ArrayList<>();
    //    animalEncounterSubItems.add(new MenuModel("Sea Lion Swim", false, false,
    // "www.google.com"));
    //    animalEncounterSubItems.add(new MenuModel("Swim with Dophins", false, false,
    // "www.google.com"));
    //    animalEncounterSubItems.add(
    //        new MenuModel("Dolphin Beach Encounter", false, false, "www.google.com"));
    //
    //    MenuModel aquarium = new MenuModel("Animal Encounter", false, true, "www.google.com");
    //    List<MenuModel> aquariumsSubItems = new ArrayList<>();
    //    aquariumsSubItems.add(new MenuModel("Sea Trek", false, false, "www.google.com"));
    //    aquariumsSubItems.add(new MenuModel("Ocean Discovery", false, false, "www.google.com"));
    //
    //    thirdLevel.put(animalEncounter, animalEncounterSubItems);
    //    thirdLevel.put(aquarium, aquariumsSubItems);
    //    List<MenuModel> thingsTodoItems = new ArrayList<>();
    //    thingsTodoItems.add(aquarium);
    //    thingsTodoItems.add(animalEncounter);
    //
    //    secondLevel.put(parent.get(parent.size() - 1), thingsTodoItems);

    //    parent.add(
    //        new MenuModel(
    //            "Getting There", true, false, "www.google.com", R.drawable.ic_menu_get_there));

    parent.add(new MenuModel("Medicine Conflicts", true, false, R.drawable.ic_menu_place_to_stay));
    parent.add(new MenuModel("First Aid", true, true, R.drawable.ic_menu_help));
    List<MenuModel> firstAidMenu = new ArrayList<>();
    firstAidMenu.add(new MenuModel("Anaphylaxis", false, false, "first_aids/anaphylaxis.txt"));
    firstAidMenu.add(new MenuModel("Bleeding", false, false, "first_aids/bleeding.txt"));
    secondLevel.put(parent.get(parent.size() - 1), firstAidMenu);
    //    parent.add(new MenuModel("News", true, false, "www.google.com", R.drawable.ic_menu_news));
    parent.add(new MenuModel("About Us", true, false, R.drawable.ic_menu_about));
    //    parent.add(new MenuModel("Social", true, false, "www.google.com",
    // R.drawable.ic_menu_social));
  }

  public List<MenuModel> getParent() {
    return parent;
  }

  public HashMap<MenuModel, List<MenuModel>> getSecondLevel() {
    return secondLevel;
  }

  public HashMap<MenuModel, List<MenuModel>> getThirdLevel() {
    return thirdLevel;
  }
}

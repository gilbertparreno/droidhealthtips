package com.droid.healthtips.util;

import android.content.Context;

import com.droid.healthtips.model.Ailment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Utils {

  public static List<Ailment> getAilmentsData(Context context) {
    Gson gson = new Gson();
    String data = getStringFromAsset(context, "ailments.json");
    if (data == null) {
      throw new NullPointerException("Can't find file from asset.");
    }

    return gson.fromJson(data, new TypeToken<List<Ailment>>() {}.getType());
  }

  public static String getStringFromAsset(Context context, String filename) {
    try {
      InputStream is = context.getAssets().open(filename);
      int size = is.available();
      byte[] buffer = new byte[size];
      is.read(buffer);
      is.close();
      return new String(buffer, "UTF-8");
    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }
  }
}

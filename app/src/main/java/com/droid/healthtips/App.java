package com.droid.healthtips;

import android.app.Application;

import com.droid.healthtips.model.Ailment;
import com.droid.healthtips.util.Utils;

import java.util.List;

public class App extends Application {

  private static App app;
  private List<Ailment> ailments;

  @Override
  public void onCreate() {
    super.onCreate();
    app = this;
    ailments = Utils.getAilmentsData(getApplicationContext());
  }

  public static App getInstance() {
    return app;
  }

  public List<Ailment> getAilments() {
    return ailments;
  }
}
